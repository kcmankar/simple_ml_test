import torch
from torch.autograd import Variable
import torch.nn as nn
import numpy as np
X=np.array([1,2,3,4,5,6,7,8,9,10],dtype=np.float32)
X=X.reshape(-1,1)
Y=np.array([1,4,9,16,25,36,49,64,81,100],dtype=np.float32)
Y=Y.reshape(-1,1)
X_tensor=Variable(torch.from_numpy(X))
Y_tensor=Variable(torch.from_numpy(Y))
class LinearRegression(nn.Module):
    def __init__(self,input_size,output_size):
        super(LinearRegression,self).__init__()
        self.Linear = nn.Linear(1,1)
    def forward(self,x):
        return self.Linear(x)
model = LinearRegression(1,1)
mse=nn.MSELoss()
lrate=0.02
optimizer = torch.optim.SGD(model.parameters(),lr=lrate)
loss_list=[]
epoch_no = 1501
for epoch in range(epoch_no):
    optimizer.zero_grad()
    results=model(X_tensor)
    loss = mse(results,Y_tensor)
    loss.backward()
    optimizer.step()
    loss_list.append(loss.data)
    if(epoch%50==0):
        print("Epoch {} loss{}".format(epoch,loss.data))


##
import matplotlib #for remote server
matplotlib.use('Agg')

import matplotlib.pyplot as plt
fig = plt.figure()
plt.plot(range(epoch_no),loss_list)
plt.xlabel("Epoch number ")
plt.ylabel("loss")
fig.savefig("Loss_graph.pdf")




