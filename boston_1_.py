#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd 
import numpy as np
from sklearn.datasets import load_boston
boston = load_boston()


# In[6]:


print(boston.data.size)


# In[7]:


print(boston.data.shape)


# In[2]:


df = pd.DataFrame(boston['data'], columns=boston['feature_names'])
df['target'] = boston['target']
df.head()


# In[5]:


print(boston.DESCR)


# In[35]:


print(boston.feature_names)


# In[26]:


y = np.array(df['AGE'])
x = np.array(df['target'])
y1 = np.array(df['CRIM'])


# In[8]:


import matplotlib.pyplot as plt


# In[31]:


plt.subplot(2,2,1)
plt.xlabel("Price of house")
plt.ylabel("AGE")
plt.scatter(x,y)
plt.subplot(2,2,3)
plt.scatter(x,y1)
plt.ylabel("Per capita Crime ")
plt.xlabel("Price of house")

plt.show()


# In[59]:


from sklearn.neighbors import  KNeighborsRegressor
clf =  KNeighborsRegressor(n_neighbors=10)


# In[60]:


from sklearn.cross_validation import train_test_split
X = df.drop('target',axis = 1)
Y = df['target']
X_train,X_test,Y_train,y_test = train_test_split(X,Y,random_state=5,test_size=0.2)


# In[61]:


clf.fit(X_train,Y_train)


# In[66]:


from sklearn.cross_validation import cross_val_score
scores = cross_val_score(clf,X_test,y_test,scoring='mean_squared_error')


# In[67]:


scores.mean()


# In[ ]:




